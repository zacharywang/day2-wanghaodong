# O

今天学习了git的使用，比如提交文件时，推荐的message写法。还有context-map的画法，和在拿到需求前先用tasking方法进行需求拆分。

过程包括需求、分析、tasking、画context map、最后才进行代码编写。

# R

difficult

# I

我认为今天学到的内容，在后面进入项目时会有很大帮助，特别是拿到需求时第一件事不是写代码，而是需要先进行分析，画context-map，理清楚逻辑。最后才进行代码的编写。有利于降低我们的代码返工率。

# D

最想将今天所学的内容应用到项目开发当中。以后进行项目开发，或者进行需求拆解时，会优先采用今天所学的context map的知识。